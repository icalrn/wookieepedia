require('dotenv').config()
const express = require('express')

const PORT = process.env.PORT || 3000
const title = 'WookieePedia'
const description = 'A Star Wars encyclopaedia microsite'
const routes = ['/', '/people', '/planets', '/vehicles']

const app = express()

app.use(express.static('dist'))

app.set('view engine', 'ejs')
app.set('views', './views')

app.get(routes, (req, res) => res.render('index', { title, description }))

app.listen(PORT, () => console.log(`App is listening on port ${PORT}`))
