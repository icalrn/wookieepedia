import People from './pages/People.vue'
import Planets from './pages/Planets.vue'
import Vehicles from './pages/Vehicles.vue'

export default [
  {
    path: '/',
    redirect: { name: 'people' }
  },
  {
    path: '/people',
    name: 'people',
    component: People
  },
  {
    path: '/planets',
    name: 'planets',
    component: Planets
  },
  {
    path: '/vehicles',
    name: 'vehicles',
    component: Vehicles
  },
]
