import axios from 'axios'

const SWAPI_HOST = 'https://swapi.co/api'

export async function getPeople(page = 1) {
  return axios.get(`${SWAPI_HOST}/people/?page=${page}`)
}

export async function getPlanets(page = 1) {
  return axios.get(`${SWAPI_HOST}/planets/?page=${page}`)
}

export async function getVehicles(page = 1) {
  return axios.get(`${SWAPI_HOST}/vehicles/?page=${page}`)
}

export default {
  getPeople,
  getPlanets,
  getVehicles
}
