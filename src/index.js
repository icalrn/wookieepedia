import 'core-js/stable'
import 'regenerator-runtime/runtime'

import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'

import App from './scripts/App.vue'

import routes from './scripts/routes'

import './index.scss'

Vue.use(VueRouter)
Vue.use(Vuex)

const router = new VueRouter({
  routes: [...routes],
  mode: 'history'
})

new Vue({
  el: '#app',
  components: {
    App
  },
  router,
  template: `
    <App />
  `
}).$mount()
